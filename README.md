# ics-ans-role-ptp

Sets up machine to be ptp master.

## Role Variables

```yaml
ptp_nic_name: em2
ptp_slave_only: 0
ptp_master_prio: 127
ptp_network_address: 192.168.3.1
ptp_subnet_mask: 225.225.225.0
ptp_phc2sys_sysconf: -a -r -r
ptp_phc2sys_log_path: /var/log/phc2sys
ptp_use_software_timestamping: false
```

## Example Playbook

```yaml
- hosts: servers
  become: true
  vars:
    ptp_nic_name: eth1
  roles:
    - role: ics-ans-role-ptp
```

## License

BSD 2-clause
